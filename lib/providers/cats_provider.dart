import 'dart:convert';
import 'package:catbreeds_pragma/screens/screens.dart';
import 'package:http/http.dart' as http;

class CatsProvider extends ChangeNotifier {
  final String _apiKey = 'bda53789-d59e-46cd-9bc4-2936630fde39';
  final String _baseUrl = 'api.thecatapi.com';

  List<dynamic> dataCats = [];

  CatsProvider() {
    getCats();
  }

  getCats() async {
    var url = Uri.https(_baseUrl, 'v1/breeds', {
      'x-api-key': _apiKey,
      'Content-Type': 'application/json;charset=UTF-8',
      'Charset': 'utf-8',
      'Accept': 'application/json',
    });

    final response = await http.get(url);
    dataCats = json.decode(response.body);
    notifyListeners();
  }
}
