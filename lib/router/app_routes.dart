import 'package:catbreeds_pragma/screens/cats_screen.dart';
import 'package:catbreeds_pragma/screens/screens.dart';

class AppRoutes {
  static const initialRoute = 'home';

  static Map<String, Widget Function(BuildContext)> getAppRoutes() {
    Map<String, Widget Function(BuildContext)> appRoutes = {};

    appRoutes.addAll({'home': (BuildContext context) => const CatsScreen()});
    appRoutes.addAll(
        {'catdetail': (BuildContext context) => const CatDetailScreen()});

    return appRoutes;
  }
}
