import 'package:flutter/material.dart';

import '../themes/app_theme.dart';

class CustomCardType1 extends StatelessWidget {
  const CustomCardType1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 10,
        shadowColor: AppTheme.primary.withOpacity(0.3),
        child: Column(
          children: [
            const ListTile(
              leading: Icon(Icons.album, color: AppTheme.primary),
              title: Text('Soy un titulo'),
              subtitle: Text(
                  'Sunt velit et tempor aliquip enim quis culpa ullamco ad dolore ea nisi aliqua minim.'),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(onPressed: () {}, child: const Text('Cancel')),
                  TextButton(onPressed: () {}, child: const Text('Ok'))
                ],
              ),
            )
          ],
        ));
  }
}
