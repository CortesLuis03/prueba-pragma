import 'package:catbreeds_pragma/themes/app_theme.dart';
import 'package:flutter/material.dart';

class CatsCards extends StatefulWidget {
  final String? catImageUrl;
  final String? catName;
  final String? catOrigin;
  final int? catIntelligence;
  final Map<String, dynamic>? dataCat;
  const CatsCards(
      {Key? key,
      required this.catImageUrl,
      this.catName,
      this.catOrigin,
      this.catIntelligence,
      this.dataCat})
      : super(key: key);

  @override
  State<CatsCards> createState() => _CatsCardsState();
}

class _CatsCardsState extends State<CatsCards> {
  @override
  Widget build(BuildContext context) {
    return Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        elevation: 10,
        shadowColor: AppTheme.primary.withOpacity(0.3),
        color: AppTheme.bgColorCards,
        child: Column(
          children: [
            ListTile(
              leading: const Icon(Icons.pets_rounded, size: 35),
              title: Text(widget.catName!),
              // trailing: const Icon(Icons.arrow_forward_ios_rounded),
              trailing: const Text(
                'Más...',
                style: TextStyle(
                    fontSize: AppTheme.textSize, color: AppTheme.linkColor),
              ),
              onTap: () {
                if (widget.catImageUrl != null) {
                  Navigator.pushNamed(context, 'catdetail',
                      arguments: widget.dataCat);
                }
              },
            ),
            FadeInImage(
              placeholder: const AssetImage('assets/loading_cat.gif'),
              image: NetworkImage(widget.catImageUrl!),
              width: double.infinity,
              fit: BoxFit.fitWidth,
              fadeInDuration: const Duration(milliseconds: 600),
            ),
            Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text('Pais de origen:',
                            style: TextStyle(
                                fontFamily: 'WorkSans',
                                fontSize: AppTheme.textSize - 2)),
                        Text(widget.catOrigin.toString(),
                            style: TextStyle(
                                color: Colors.black.withOpacity(0.6),
                                fontFamily: 'WorkSans',
                                fontSize: AppTheme.textSize - 2))
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Text('Inteligencia:',
                            style: TextStyle(
                                fontFamily: 'WorkSans',
                                fontSize: AppTheme.textSize - 2)),
                        Text(widget.catIntelligence.toString(),
                            style: TextStyle(
                                color: Colors.black.withOpacity(0.6),
                                fontFamily: 'WorkSans',
                                fontSize: AppTheme.textSize - 2))
                      ],
                    )
                  ],
                ))
          ],
        ));
  }
}
