import 'package:catbreeds_pragma/providers/cats_provider.dart';
import 'package:catbreeds_pragma/router/app_routes.dart';
import 'package:catbreeds_pragma/screens/cats_screen.dart';
import 'package:catbreeds_pragma/themes/app_theme.dart';
import 'package:provider/provider.dart';

import 'screens/screens.dart';

void main() => runApp(const AppState());

class AppState extends StatelessWidget {
  const AppState({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => CatsProvider(), lazy: false),
      ],
      child: const MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: AppRoutes.initialRoute,
      routes: AppRoutes.getAppRoutes(),
      onGenerateRoute: (settings) {
        return MaterialPageRoute(builder: (context) => const CatsScreen());
      },
      theme: AppTheme.lightTheme,
    );
  }
}
