import 'package:catbreeds_pragma/themes/app_theme.dart';
import 'package:flutter/material.dart';

class CatDetailScreen extends StatelessWidget {
  const CatDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic>? data =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>?;
    return Scaffold(
      backgroundColor: AppTheme.bgColor,
      appBar: AppBar(
        title: Text(
          data!['name'],
          style: const TextStyle(color: Colors.white, fontFamily: 'WorkSans'),
        ),
      ),
      body: Column(
        children: [
          Container(
              padding: const EdgeInsets.all(0),
              height: data['image'] != null
                  ? ((data['image']['height'] / data['image']['width']) > 1.1
                      ? MediaQuery.of(context).size.height / 2.2
                      : null)
                  : null,
              child: FadeInImage(
                image: NetworkImage(data['image'] != null
                    ? data['image']['url']
                    : 'https://static.vecteezy.com/system/resources/previews/004/447/618/original/design-layout-for-the-404-error-page-not-found-with-a-silhouette-of-a-cat-on-the-background-of-a-window-vector.jpg'),
                placeholder: const AssetImage('assets/loading_cat.gif'),
              )),
          Expanded(
              child: Scrollbar(
            thickness: 7,
            child: SingleChildScrollView(
              // for Vertical scrolling
              scrollDirection: Axis.vertical,
              padding: const EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    subtitle: Text(data['description'],
                        textAlign: TextAlign.justify,
                        style: const TextStyle(
                          color: AppTheme.textColor,
                          fontFamily: 'WorkSans',
                          fontSize: AppTheme.textSize,
                        )),
                    minLeadingWidth: 0,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    children: [
                      const Text(
                        "Origen:",
                        style: TextStyle(
                            color: AppTheme.textColor,
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                      Text(
                        data['origin'],
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Temperamento:",
                        style: TextStyle(
                            color: AppTheme.textColor,
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                      Text(
                        data['temperament'],
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                        softWrap: true,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    spacing: 8,
                    children: [
                      const Text(
                        "Nivel de inteligencia:",
                        style: TextStyle(
                            color: AppTheme.textColor,
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                      Text(
                        data['intelligence'].toString(),
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    spacing: 8,
                    children: [
                      const Text(
                        "Nivel de adaptabilidad:",
                        style: TextStyle(
                            color: AppTheme.textColor,
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                      Text(
                        data['adaptability'].toString(),
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    spacing: 8,
                    children: [
                      const Text(
                        "Tiempo de Vida (años):",
                        style: TextStyle(
                            color: AppTheme.textColor,
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                      Text(
                        data['life_span'],
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.start,
                    spacing: 8,
                    children: [
                      const Text(
                        "Nivel de afecto:",
                        style: TextStyle(
                            color: AppTheme.textColor,
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                      Text(
                        data['affection_level'].toString(),
                        style: TextStyle(
                            color: Colors.black.withOpacity(0.6),
                            fontFamily: 'WorkSans',
                            fontSize: AppTheme.textSize),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ))
        ],
      ),
    );
  }
}
