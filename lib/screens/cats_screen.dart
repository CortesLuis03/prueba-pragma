import 'package:catbreeds_pragma/providers/cats_provider.dart';
import 'package:catbreeds_pragma/themes/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/widgets.dart';

class CatsScreen extends StatefulWidget {
  const CatsScreen({Key? key}) : super(key: key);

  @override
  State<CatsScreen> createState() => _CatsScreenState();
}

class _CatsScreenState extends State<CatsScreen> {
  final ScrollController scrollController = ScrollController();
  final fieldSearch = TextEditingController();
  bool toTop = false;
  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      scrollController.position.pixels >= 6000 ? fetchData() : toTop = false;
      setState(() {});
    });
  }

  Future fetchData() async {
    if (toTop) return;

    toTop = true;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    List catsProvider =
        Provider.of<CatsProvider>(context, listen: true).dataCats;

    void searchCat(String query) {
      // final query = fieldSearch.value.toString();
      final suggestions = catsProvider.where((cat) {
        final catName = cat['origin'].toLowerCase();
        final input = query.toString().toLowerCase();
        return catName.contains(input);
      }).toList();
      if (suggestions.isEmpty && query.isEmpty ||
          suggestions.isNotEmpty && query.isEmpty) {
        Provider.of<CatsProvider>(context, listen: false).getCats();
      } else {
        setState(() {
          catsProvider.clear();
          catsProvider.addAll(suggestions);
        });
      }
    }

    void clearSearch() {
      fieldSearch.clear();
      Provider.of<CatsProvider>(context, listen: false).getCats();
      FocusManager.instance.primaryFocus?.unfocus();
    }

    return Scaffold(
      backgroundColor: AppTheme.bgColor,
      appBar: AppBar(
        title: const Text('Catbreeds'),
      ),
      body: GestureDetector(
        onTap: () {
          final FocusScopeNode focus = FocusScope.of(context);
          if (!focus.hasPrimaryFocus && focus.hasFocus) {
            FocusManager.instance.primaryFocus?.unfocus();
          }
        },
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(
                  top: 20, right: 15, left: 15, bottom: 10),
              child: TextField(
                decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  prefixIcon: const Icon(Icons.search),
                  hintText: 'Cat search',
                  // suffixIcon: const Icon(Icons.clear),
                  suffixIcon: IconButton(
                      icon: const Icon(Icons.clear), onPressed: clearSearch),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                      borderSide: const BorderSide(color: Colors.black)),
                ),
                controller: fieldSearch,
                onChanged: searchCat,
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  ListView.builder(
                    controller: scrollController,
                    itemCount: catsProvider.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          CatsCards(
                              catImageUrl: catsProvider[index]['image'] ==
                                          null ||
                                      catsProvider[index]['image'].length == 0
                                  ? 'https://static.vecteezy.com/system/resources/previews/004/447/618/original/design-layout-for-the-404-error-page-not-found-with-a-silhouette-of-a-cat-on-the-background-of-a-window-vector.jpg'
                                  : catsProvider[index]['image']['url'],
                              catName: catsProvider[index]['name'],
                              catOrigin: catsProvider[index]['origin'],
                              catIntelligence: catsProvider[index]
                                  ['intelligence'],
                              dataCat: catsProvider[index]),
                          const SizedBox(
                            height: 20,
                          )
                        ],
                      );
                    },
                    padding:
                        const EdgeInsets.only(top: 10, left: 10, right: 10),
                  ),
                  if (toTop)
                    Positioned(
                        bottom: 40,
                        left: size.width / 2 - 30,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              fixedSize: const Size(50, 65),
                              primary:
                                  const Color.fromRGBO(110, 198, 255, 0.9)),
                          onPressed: () {
                            scrollController.animateTo(0,
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.fastOutSlowIn);
                          },
                          child: Center(
                            child:
                                Image.asset('assets/arrow_up.png', width: 25),
                          ),
                        ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
