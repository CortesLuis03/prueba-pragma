import 'package:flutter/material.dart';

class AppTheme {
  static const Color primary = Color.fromRGBO(25, 118, 210, 1);
  static const Color bgColor = Color.fromRGBO(240, 240, 240, 1);
  static const Color bgColorCards = Color.fromARGB(255, 255, 255, 255);
  static const Color textColor = Color.fromRGBO(48, 70, 65, 1);
  static const Color linkColor = Color.fromRGBO(25, 118, 210, 1);
  static const double textSize = 18;

  static final ThemeData lightTheme = ThemeData.light().copyWith(
      primaryColor: primary,
      appBarTheme:
          const AppBarTheme(color: primary, elevation: 5, centerTitle: true),
      elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
              primary: primary, shape: const StadiumBorder(), elevation: 0)),
      inputDecorationTheme: const InputDecorationTheme(
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          focusColor: Colors.black));
}
